/*
 * Thrift definition for Control service
 */

namespace d rpc

/**
 * Represents the resulting status of an operation
 */
enum Status {
  SUCCESS = 1,
  FAIL = 2
}

/**
 * RPC implementation of lzc_send_flags
 */
enum rpc_SendFlags {
  LZC_SEND_FLAG_LARGE_BLOCK,
  LZC_SEND_FLAG_EMBED_DATA,
  LZC_SEND_FLAG_COMPRESS,
  LZC_SEND_FLAG_RAW
}

/**
 * rpc_SendRequest
 * written to use lzc_send directly
 * TODO: fix libzfs_core_d issues
 */
struct rpc_SendRequest {
  1: string snapname,
  2: string fromsnap,
  3: rpc_Server destserver,
  4: rpc_SendFlags sendflags,
  5: string txUUID
}

/**
 * rpc_RecvRequest
 * written to use lzc_receive directly
 * TODO: fix libzfs_core_d issues and rewrite to include 'props'
 */
struct rpc_RecvRequest {
  1: string snapname,
  2: string origin,
  3: rpc_Server originserver,
  4: bool force,
  5: bool raw,
  6: string txUUID
}

/**
 * Zpool struct.
 */
struct rpc_Zpool {
  1: string _id,
  2: string name,
  3: int zStatus,
  4: string serverId
}

/**
 * Zpool struct.
 */
struct rpc_Server {
  1: string _id,
  2: string name,
  3: string fqdn,
  4: string machId
}

/**
 * Dataset struct.
 */
struct rpc_Dataset {
  1: string _id,
  2: string name,
  3: int zStatus,
  4: int backupStatus,
  5: int backupsEnabled,
  6: string fullname,
  7: string serverId
}

/**
 * Requested operation has failed.
 */
exception OperationFailed {
  1: i32 whatOp,
  2: string why
}

/**
 * Operation cannot be completed because the backupd daemon is stopped.
 */
exception ServiceNotRunning {
  1: i32 whatOp,
  2: string why
}

/**
 * Resuested operation is somehow invalid.
 */
exception InvalidOperation {
  1: i32 whatOp,
  2: string why
}

/**
 * Control RPC Service
 */
service Control {

  /**
   * A method definition looks like C code. It has a return type, arguments,
   * and optionally a list of exceptions that it may throw. Note that argument
   * lists and exception lists are specified using the exact same syntax as
   * field lists in struct or exception definitions.
   */

  /**
   * Dummy function for testing
   */
  void ping(),

  /**
   * Shutdown backupd program
   */
  void shutdown(),

  rpc_Zpool getZpoolByName(1:string name),

  rpc_Dataset getDatasetByName(1:string name),

  rpc_Zpool getZpoolById(1:i32 id),

  rpc_Dataset getDatasetById(1:i32 id),

  /**
   * Start backupd service
   */
  Status startService(),

  /**
   * Stop backupd service
   */
  Status stopService(),

  Status enableZpool(1:string name),

  Status enableDataset(1:string name),

  Status regitsterServer(1:rpc_Server server),

  Status regitsterRpcServer(),

  Status regitsterServerVolumes(),

  Status sendSnapshot(1:rpc_SendRequest sendreq),

  Status recvSnapshot(1:rpc_RecvRequest recvreq),

  // Status sendSnapshotIncremental(1:string snapname, 2:string fromsnap, 3:string destserver),

  // Status recvSnapshotIncremental(1:string snapname, 2:string fromsnap, 3:string destserver),

  i32 createZpool(1:string name, 2:i32 serverId) throws (1:OperationFailed e),

  i32 createDataset(1:string name, 2:i32 parentId) throws (1:OperationFailed e),

  oneway void zip()

}
