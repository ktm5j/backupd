import std.stdio;
import std.getopt;
import std.file;
import std.string: split, strip;
import std.uuid;

import core.stdc.stdlib : exit;

import thrift.base;
import thrift.codegen.client;
import thrift.protocol.binary;
import thrift.transport.buffered;
import thrift.transport.socket;

import vibe.core.log;

import rpc.Control;
import rpc.rpc_types;

/// hostname of endpoint server
package string server = "localhost";
package int port = 8535;
package bool help = false;

package Control rpcClient;

alias logInfo = vibe.core.log.logInfo;
alias exit = core.stdc.stdlib.exit;

void main(string[] args) {
    string txId = "";
    bool help;
    getopt(args,
           "h|help",   &help,
           "s|server", &server,
           "p|port",   &port,
           "tx",       &txId
           );

    logInfo("[main] txid: %s", txId);

    if (args.length < 2)
        throw new Exception("Must enter subcommand");

    auto subcommand = args[1];
    switch (subcommand)
    {
    case "register":
        registerRpcAction();
        break;

    case "exit":
        initRpcClient();
        rpcClient.shutdown();
        break;

    case "enable":
        enableAction(args);
        break;

    case "send":
        sendAction(args, txId);
        break;

    case "recv":
        recvAction(args, txId);
        break;

    case "start":
        startAction();
        break;

    case "stop":
        stopAction();
        break;

    default:
        throw new Exception("Unknown subcommand " ~ subcommand);
    }
}

void evaluateStatus(Status s) {
    switch (s) {
    case Status.SUCCESS:
        logInfo("Success");
        break;

    case Status.FAIL:
        logInfo("Failure");
        break;

    default:
        logInfo("Failure");
        break;
    }
}

void startAction() {
    initRpcClient();
    Status s = rpcClient.startService();

    evaluateStatus(s);
}

void stopAction() {
    initRpcClient();
    Status s = rpcClient.stopService();

    evaluateStatus(s);
}

void enableAction(string[] args) {
    if (args.length < 3)
        throw new Exception("Not enough args");

    writeln(args[2]);
    initRpcClient();
    Status s = rpcClient.enableDataset(args[2]);

    switch (s) {
    case Status.SUCCESS:
        logInfo("Success");
        break;

    case Status.FAIL:
        logInfo("Failure");
        break;

    default:
        logInfo("Failure");
        break;
    }
}

void initRpcClient() {
    auto socket = new TSocket(server, cast(ushort)port);
    auto transport = new TBufferedTransport(socket);
    auto protocol = tBinaryProtocol(transport);
    rpcClient = tClient!Control(protocol);

    transport.open();
}

void registerAction() {
    initRpcClient();

    rpc_Server srv = rpc_Server("", getSystemHostname, getSystemFqdn, getSystemMachId);

    rpcClient.regitsterServer(srv);
    // rpcClient.ping();
}

void registerRpcAction() {
    initRpcClient();

    rpcClient.regitsterRpcServer();
    // rpcClient.ping();
}


void sendAction(string[] args, string txid) {
    writeln(txid);
    if (args.length < 4)
        throw new Exception("Not enough args");

    try {
        string s = txid;
        auto x = parseUUID(s);
    } catch (Exception e) {
        throw new Exception("Error parsing txid");
    }

    initRpcClient();

    rpc_Server dest = rpc_Server();
    dest.fqdn = args[3];

    logInfo("txid: %s", txid);

    rpc_SendRequest req = rpc_SendRequest();
    req.snapname = args[2];
    req.destserver = dest;
    req.txUUID = txid;

    if (args.length > 4)
        req.fromsnap = args[4];
    else
        req.fromsnap = null;

    Status ret = rpcClient.sendSnapshot(req);

    if (ret == Status.SUCCESS) {
        logInfo("success");
        exit(0);
    }

    if (ret == Status.FAIL) {
        logInfo("fail");
        exit(1);
    }
}

void recvAction(string[] args, string txid) {
    if (args.length < 4)
        throw new Exception("Not enough args");

    try {
        string s = txid;
        auto x = parseUUID(s);
    } catch (Exception e) {
        throw new Exception("Error parsing txid");
    }

    initRpcClient();

    rpc_Server origin = rpc_Server();
    origin.fqdn = args[3];

    rpc_RecvRequest req = rpc_RecvRequest();
    req.snapname = args[2];
    req.originserver = origin;
    req.txUUID = txid;

    if (args.length > 4)
        req.origin = args[4];
    else
        req.origin = null;

    Status ret = rpcClient.recvSnapshot(req);

    if (ret == Status.SUCCESS) {
        logInfo("success");
        exit(0);
    }

    if (ret == Status.FAIL) {
        logInfo("fail");
        exit(1);
    }
}

public string getSystemMachId() {
    try {
        string fn = "/etc/machine-id";
        return readText(fn).strip();
    } catch (FileException e) {
        return "";
    }
}

public string getSystemHostname() {
    try {
        string fn = "/etc/hostname";

        string[] parts = readText(fn).split(".");
        return parts[0].strip();
    } catch (FileException e) {
        return "";
    }
}

public string getSystemDomain() {
    try {
        string resolvText = readText("/etc/resolv.conf");
        string[] lines = resolvText.split("\n");

        for (int i = 0; i < lines.length; i++) {
            string[] parts = lines[i].split(" ");

            if (parts.length > 1 && parts[0] == "domain") {
                return parts[1];
            }

            if (parts.length > 1 && parts[0] == "search") {
                return parts[1];
            }
        }

        return "";
    } catch (FileException e) {
        return "";
    }
}

public string getSystemFqdn() {
    return getSystemHostname() ~ "." ~ getSystemDomain();
}
