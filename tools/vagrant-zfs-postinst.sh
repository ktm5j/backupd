#!/bin/bash

#
#  Vagrant provisioning support script
#

# Script to be run as root after reboot following successful provisioning

# If zfs module is not loaded then try loading
# If loading fails then run dracut autoinstall
# If modprobe still fails the exit 1
if ! lsmod | grep zfs 2>&1 > /dev/null
then
    if ! modprobe zfs
    then
        dracut autoinstall --force

        modprobe zfs || exit 1
    fi
fi

zpool create -o ashift=9 test /dev/vdb
