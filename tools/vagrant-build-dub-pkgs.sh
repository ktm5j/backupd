#!/bin/bash

#
#  Vagrant provisioning support script
#

# To be run by `vagrant` user after first boot following provisioning

source /opt/dlang/dmd-2.090.0/activate

dub fetch dpp

sed -i 's/local\/clang-7.0.0/lib64\/llvm7.0/g' ~/.dub/packages/dpp-*/dpp/dub.json

dub build dpp && sudo cp ~/.dub/packages/dpp-*/dpp/bin/d++ /usr/bin
