#!/bin/bash

SRC=/z/Code/D/backupd-v2

cd ${SRC}/thrift

thrift --gen d rpc.thrift && \
	cp -a ${SRC}/thrift/gen-d/rpc ${SRC}/source && \
	sed -i 's/void main/void initRpc/g' ${SRC}/source/rpc/Control_server.skeleton.d
