module text;

import std.format;
import std.stdio;

void main() {

    string s = format!"fmt: %s"("test");

    writeln(s);

}
