module testing;

import std.json;
import std.stdio;
import std.file;

import vibe.d;
import vibe.data.bson;
import vibe.data.json;
import vibe.core.log;

import zfs;
import data;

DbConf dbconf = {"ktm5j", "f00bar", "127.0.0.1", 27017, "backupd"};

unittest
{
    writeln("Running first unittest!");

    assert(true);
}

void main() {
    setConfig(dbconf);
    MongoClient db = getDbClient();

    Server localhost = getServerMach(db, getMachId());
    logWarn(localhost.name);

    // Dataset ds = getDataset(db, BsonObjectID.fromString("5dcee103555a36fa6cfe9da4"));

    Dataset ds = getDataset(db, "z/Code/Mozilla", BsonObjectID.fromString("5dc443c20e4dc45513c7102d"));

    ds.backupsEnabled = 1;
    updateItem(db, ds);
}

public string getMachId() {
    try {
        string fn = "/etc/machine-id";
        return readText(fn).strip();
    } catch (FileException e) {
        return "";
    }
}
