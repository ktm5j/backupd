/**
 * Module containing global scope variables
 *
 * Used to house pointer to main `Controller` class as well as runtime flags
 */

module globals;

import system;
import models;
import transport;

import std.concurrency : receive, receiveOnly,
    send, spawn, thisTid, Tid;

/**
 * Flag to indicate test run
 */
__gshared bool g_flag_test;

/**
 * Flag to enable debug messages
 *
 * Will enable printing messages via `vibe.core.log` from methods such as
 * `logDebug()`
 */
__gshared bool g_flag_debug;

/**
 * Represents if main services are running or not
 *
 * Set to true to start backup services. Many RPC methods will fail without this
 * enabled
 */
__gshared static bool g_service_running = true;

/**
 * Thread id of controller
 *
 * Used for passing messages from RPC to control
 */
__gshared Tid ControlThread;

/**
 * Main program Controller Class
 *
 * This Controller drives the main program.  Will initiate backups.
 */
__gshared Controller control;

__gshared static BackupdConfig AppConfig;
