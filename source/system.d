/**
 * The system module provides functions and classes for interacting with
 * the local operating system.
 *
 * Houses the Controller class, which drives the main program flow.
 * */

module system;

import std.file : readText, FileException;
import std.process;
import std.stdio;
import std.string : split;

import core.stdc.stdlib : exit;
alias exit = core.stdc.stdlib.exit;

import vibe.core.log;

import zfs;
import data;
import globals;
import util.simpletime;
import transport;

/**
 * Controller 
 * */
public class Controller {

    public static Server localhost;
    private MongoClient db;

    public ushort ioPort = 9954;

    this() {
        this.db = getDbClient();

        this.localhost = getServerMach(db, getSystemMachId());
        logWarn(this.localhost.name);

        logDebug("opening transport socket");
        OpenTransportSocket();

        logDebug("starting transport system");
        StartTransport();
    }

    this(ushort _ioPort) {
        this();
        this.ioPort = _ioPort;
    }

    public ushort getIoPort() {
        return this.ioPort;
    }

    public void setIoPort(ushort _ioPort) {
        this.ioPort = _ioPort;
    }

    ~this() {
        closeClient(db);
    }

    public void shutdown() {
        writefln("shutdown called %s", localhost.name);
        exit(0);

    }

    public void registerLocalVolumes() {
        ZCollection zc = getZcollection(localhost._id);

        insertZCollection(db, zc);
    }

    public void pruneLocalVolumes() {
        MongoCursor!Dataset datasets = getServerDatasets(db, localhost._id);

        foreach (ds; datasets) {
            if (! datasetExists(ds.fullname) )
                removeDataset(db, ds);
        }

    }

    public void pruneLocalSnapshots() {
        MongoCursor!Dataset datasets = getServerDatasets(db, localhost._id);

        foreach (ds; datasets) {
            MongoCursor!Snapshot snapshots  = getDatasetSnapshots(db, ds);

            // If dataset doesn't exist then neither do it's snapshots
            // quick kill all without testing existence of each
            if (! datasetExists(ds.fullname) ) {
                removeDataset(db, ds);
                foreach (ss; snapshots)
                    removeSnapshot(db, ss);

                continue;
            }

            foreach (ss; snapshots) {
                if (! datasetExists(ss.snapname))
                    removeSnapshot(db, ss);
            }
        }

    }

    public void registerSnapshots() {
        MongoCursor!Dataset datasets = getServerDatasets(db, localhost._id);

        foreach (ds; datasets) {
            if (g_flag_debug)
                logInfo("grabbing snaps for: %s", ds.fullname);

            Snapshot[] sss = getSnapshots(ds);
            foreach (ss; sss) {
                insertSnapshot(db, ss);
            }
        }

    }

    public void createBackupPlan() {
        BackupPlan bp = BackupPlan();
    }

}

unittest {
    setConfig( DbConf("ktm5j", "f00bar", "127.0.0.1", 27017, "backupd"));
    auto db = getDbClient();

    Server localhost = getServerMach(db, getSystemMachId());

    logWarn("Running sanity test..");

    assert(localhost.machId == getSystemMachId());

    assert(localhost.name == getSystemHostname());

    assert(localhost.fqdn == getSystemFqdn());

    closeClient(db);
}

public string getSystemMachId() {
  try {
    string fn = "/etc/machine-id";
    return readText(fn).strip();
  } catch (FileException e) {
    return "";
  }
}

public string getSystemHostname() {
  try {
    string fn = "/etc/hostname";

    string[] parts = readText(fn).split(".");
    return parts[0].strip();
  } catch (FileException e) {
    return "";
  }
}

public string getSystemDomain() {
  try {
    string resolvText = readText("/etc/resolv.conf");
    string[] lines = resolvText.split("\n");

    for (int i = 0; i < lines.length; i++) {
      string[] parts = lines[i].split(" ");

      if (parts.length > 1 && parts[0] == "domain") {
        return parts[1];
      }

      if (parts.length > 1 && parts[0] == "search") {
        return parts[1];
      }
    }

    return "";
  } catch (FileException e) {
    return "";
  }
}

public string getSystemFqdn() {
    return getSystemHostname() ~ "." ~ getSystemDomain();
}

public string getLocalHostIP() {
    return "0.0.0.0";
}

unittest {
    assert(getSystemDomain() == "cs.virginia.edu");

    assert(getSystemHostname() == "kurma");

    assert(getSystemFqdn() == "kurma.cs.virginia.edu");

    assert(getSystemMachId() == "258bb5b469aa93323967881a80ac0227");
}
