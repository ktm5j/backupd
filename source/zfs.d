module zfs;

public import symmetry.api.libzfs_core;
import vibe.core.log;
import vibe.db.mongo.mongo : BsonObjectID;

import std.array;
import std.exception;
import std.json;
import std.string;
import std.socket;

import std.concurrency;

import globals;

import rpc.rpc_types;

import models;

ZCollection getZcollection(BsonObjectID serverId) {
    logWarn(serverId.toString());
    ZCollection zc = ZCollection();

    string[] zlist = zListAll();

    for (int i = 0; i < zlist.length; i++) {
        string[] parts = zlist[i].split("/");

        if (parts.length == 1) {
            BsonObjectID bid = BsonObjectID.fromString(serverId.toString());

            Zpool pool = Zpool(parts[0], bid);
            pool.genId();

            zc.add(pool);
        } else {
            BsonObjectID bid = BsonObjectID.fromString(serverId.toString());

            Dataset dataset =
                Dataset(parts[ parts.length - 1 ],
                        zlist[i],
                        bid);
            dataset.genId();

            zc.add(dataset);
        }

    }

    return zc;
}

public Snapshot[] getSnapshots(Dataset ds) {
    string[] snaps = listSnapshots(ds.fullname, false, false);

    Snapshot[] sss;
    sss.length = snaps.length;

    for (int i = 0; i < sss.length; i++) {
        sss[i] = Snapshot(snaps[i], ds.serverId);
    }

    return sss;
}

public Tid sendSnapAsync(string snapname, string fromsnap, string destserve, Tid parentT) {
    return spawn(&sendSnapAsync_impl, snapname, fromsnap, destserve, parentT);
}

private void sendSnapAsync_impl(string snapname, string fromsnap, string destserve, Tid parentT) {
    logDebug("rpc: send request init");

    int ret = 0;
    Socket server = new TcpSocket();
    server.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, true);
    Socket client;
    try {

        server.bind(new InternetAddress(InternetAddress.ADDR_ANY, control.ioPort));

        server.listen(10);
        server.blocking(true);

        send(parentT, Status.SUCCESS);
        client = server.accept();
        logDebug("rpc: client connected");

        if (fromsnap == null)
            ret = lzc_send(snapname.toCString, null, cast(int)client.handle, cast(lzc_send_flags)0);
        else
            ret = lzc_send(snapname.toCString, fromsnap.toCString, cast(int)client.handle, cast(lzc_send_flags)0);
        logDebug("rpc: send process finished");
    } catch (Exception e) {
        logError("error: %d from zfs.sendSnapAsync_impl: %s", ret, e.msg);
    }

    client.close();
    server.close();
}

public Status sendSnap_impl(string snapname, string fromsnap, string destserve, Socket client) {
    logDebug("rpc: send request init");

    int ret = 0;
    try {
        logDebug("rpc: client connected");

        if (fromsnap == null)
            ret = lzc_send(snapname.toCString, null, cast(int)client.handle, cast(lzc_send_flags)0);
        else
            ret = lzc_send(snapname.toCString, fromsnap.toCString, cast(int)client.handle, cast(lzc_send_flags)0);
        logDebug("rpc: send process finished");
    } catch (Exception e) {
        logError("error: %d from zfs.sendSnap_impl: %s", ret, e.msg);
        return Status.FAIL;
    }

    client.close();
    return Status.SUCCESS;
}

public Tid recvSnapAsync(string snapname, string origin, string destserve, Tid parentT) {
    return spawn(&recvSnapAsync_impl, snapname, origin, destserve, parentT);
}

private void recvSnapAsync_impl(string snapname, string origin, string destserve, Tid parentT) {
    logDebug("rpc: recv request init");

    static int ret = 0;
    Socket server = new TcpSocket();
    server.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, true);
    try {

        send(parentT, Status.SUCCESS);
        server.connect(new InternetAddress(destserve, control.ioPort));
        if (origin == null)
            ret = lzc_receive(snapname.toCString, null, null, 1, 0, cast(int)server.handle);
        else
            ret = lzc_receive(snapname.toCString, null, origin.toCString, 1, 0, cast(int)server.handle);
        logDebug("rpc: recv process finished");
    } catch (Exception e) {
        logError("error: %d from zfs.recvSnapAsync_impl: %s", ret, e.msg);
    }

    server.close();
}

public Status recvSnap_impl(string snapname, string origin, string destserve, Socket server) {
    logDebug("rpc: recv request init");

    static int ret = 0;
    try {
        if (origin == null)
            ret = lzc_receive(snapname.toCString, null, null, 1, 0, cast(int)server.handle);
        else
            ret = lzc_receive(snapname.toCString, null, origin.toCString, 1, 0, cast(int)server.handle);
        logDebug("rpc: recv process finished");
    } catch (Exception e) {
        logError("error: %d from zfs.recvSnap_impl: %s", ret, e.msg);
        return Status.FAIL;
    }

    server.close();
    return Status.SUCCESS;
}

unittest {
    string[] snaps = listSnapshots("z/testing", true, false);
    for (int i = 0; i < snaps.length; i++) {
        logInfo(snaps[i]);
    }
}

string[] zListAll() {
    return listChildren("", false);
}

