module util.simpletime;

import std.stdio;
import std.algorithm.mutation;

import std.datetime;
import std.datetime.date : DateTime, TimeOfDay;
import std.datetime.timezone : LocalTime;
import std.datetime.systime;

import std.concurrency : receive, receiveOnly, receiveTimeout, send, spawn, thisTid, Tid;


/**
 * Simple Timer Class
 */
class Timer
{
    this(void function() _action)
    {
        this.action = _action;
    }

public:

    void addTime(TimeOfDay _time)
    {
        this.scheduledTimes ~= _time;

        if (this.running) {
	        ScheduleMessage msg = ScheduleMessage(this.scheduledTimes.dup);
	        send(this.actionThread, msg);
	    }
    }

    void removeTime(TimeOfDay _time)
    {
    	for (int i = 0; i < this.scheduledTimes.length; i++)
    		if (this.scheduledTimes[i] is _time)
    			this.scheduledTimes = this.scheduledTimes.remove(i);

		if (this.running) {
	        send(this.actionThread, ScheduleMessage(this.scheduledTimes));
	    }
    }

    void run()
    {
    	if (this.running)
    		return;

        this.running = true;
        this.actionThread = spawn(&timerThread, thisTid);

        ScheduleMessage msg = ScheduleMessage(this.scheduledTimes.dup);
        send(this.actionThread, msg);
    }

    void stop()
    {
    	if (!this.running)
    		return;

    	send(this.actionThread, CancelMessage());
    	receiveOnly!CancelAckMessage;
    	this.running = false;
    }

private:
	struct ScheduleMessage
	{
	    shared TimeOfDay[] scheduledTimes;
	    this(TimeOfDay[] st)
	    {
	        this.scheduledTimes = cast(shared)st;
	    }
	}

	/*
	Message is used as a stop sign for other
	threads
	*/
	struct CancelMessage {
	}

	/// Acknowledge a CancelMessage
	struct CancelAckMessage {
	}

    Tid actionThread;
    TimeOfDay[] scheduledTimes;

    __gshared void function() action;
    bool running;

    static void timerThread(Tid parentId)
    {
    	bool canceled = false;
        shared TimeOfDay[] _scheduledTimes;

        while (!canceled)
        {
            foreach (time; _scheduledTimes) {
            	auto tod = (cast(DateTime)Clock.currTime()).timeOfDay();
	            if (cast(shared)tod is time)
	            	action();
	        }
            receiveTimeout( dur!("seconds")( 1 ),
            	(ScheduleMessage m) { 
            		writeln("Received schedule");
            		_scheduledTimes = m.scheduledTimes;
            	},
	            (CancelMessage m) {
	                canceled = true;
	                writeln("Stopping ", thisTid, "...");
	                send(parentId, CancelAckMessage());
	            }
            );
        }
    }
}
