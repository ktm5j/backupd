/// Backupd main entry point

import data;
import zfs;
import system;
import rpc.Control_server;

import globals;

import models: BackupdConfig, DatabaseConfig, RpcConfig, ZfsConfig;

import core.thread;

import std.algorithm;
import std.getopt;
import std.stdio;
import std.string;

import core.stdc.stdlib : exit;
alias exit = core.stdc.stdlib.exit;

import std.concurrency : receive, receiveOnly,
    send, spawn, thisTid, Tid;

private string bindAddr = "localhost";
private int port = 8535;

private bool help = false;

package bool flag_test = false;
package bool flag_debug = false;
package bool flag_register = false;

private string logFile = "/tmp/backupd.log";
private bool foreground = false;

private string dbDriver = "postgresql";
private string dbAddr = "127.0.0.1";
private ushort dbPort = 27017;
private string dbDatabase = "backupd";
private string dbUsername = "ktm5j";
private string dbPassword = "f00bar";

private ushort ioPort = 9954;
private ushort rpcPort = 8864;

private string ioBind = "0.0.0.0";

unittest {
    writeln("test");
}

void main(string[] args)
{
    getopt(args,
           "h|help",         &help,

           "d|debug",        &flag_debug,
           "t|test",         &flag_test,
           "register",       &flag_register,

           "b|bind",         &bindAddr,
           "p|port",         &port,
           "l|log",          &logFile,
           "f|foreground",   &foreground,

           "dbdriver",       &dbDriver,
           "dbaddress",      &dbAddr,
           "dbport",         &dbPort,
           "D|dbdatabase",   &dbDatabase,
           "U|dbusername",   &dbUsername,
           "P|dbpassword",   &dbPassword,

           "i|ioport",       &ioPort,
           "iobind",         &ioBind

           );

    setLogLevel(LogLevel.error);

    writeln("Initializing DB connection");

    AppConfig = BackupdConfig();

    AppConfig.databaseConfig = DatabaseConfig(dbUsername, dbPassword, dbAddr, dbPort, dbDatabase);

    AppConfig.zfsConfig = ZfsConfig(ioBind, ioPort);

    g_flag_test = flag_test;
    g_flag_debug = flag_debug;

    if (flag_debug)
        setLogLevel(LogLevel.debug_);

    if (flag_register) {
        initRpc();
        exit(0);
    }

    control = new Controller(ioPort);

    control.registerLocalVolumes();
    control.pruneLocalSnapshots();
    control.pruneLocalVolumes();
    control.registerSnapshots();

    if (flag_test) {
        import rpc.RpcClient;
        import rpc.Control;

        Control client = initRpcClient("10.192.43.250", 8535);

        client.ping();

        exit(0);
    }

    initRpc();
}
