module rpc.RpcClient;

import rpc.Control;
import rpc.rpc_types;

import thrift.base;
import thrift.codegen.client;
import thrift.protocol.binary;
import thrift.transport.buffered;
import thrift.transport.socket;

Control initRpcClient(string addr, ushort port) {
    auto socket = new TSocket(addr, port);
    auto transport = new TBufferedTransport(socket);
    auto protocol = tBinaryProtocol(transport);
    Control rpcClient = tClient!Control(protocol);

    transport.open();
    return rpcClient;
}
