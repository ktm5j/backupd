/*
 * This auto-generated skeleton file illustrates how to build a server. If you
 * intend to customize it, you should edit a copy with another file name to 
 * avoid overwriting it when running the generator again.
 */
module rpc.Control_server;

import std.stdio;
import thrift.codegen.processor;
import thrift.protocol.binary;
import thrift.server.simple;
import thrift.server.transport.socket;
import thrift.transport.buffered;
import thrift.util.hashset;

import rpc.Control;
import rpc.rpc_types;

import rpc.Control_server_impl;

class ControlHandler : Control {
  this() {
    // Your initialization goes here.
  }

  void ping() {
    // Your implementation goes here.
    writeln("ping called");
  }

  void shutdown() {
        shutdown_impl();
  }

  rpc_Zpool getZpoolByName(string name) {
    // Your implementation goes here.
    writeln("getZpoolByName called");
    return typeof(return).init;
  }

  rpc_Dataset getDatasetByName(string name) {
    // Your implementation goes here.
    writeln("getDatasetByName called");
    return typeof(return).init;
  }

  rpc_Zpool getZpoolById(int id) {
    // Your implementation goes here.
    writeln("getZpoolById called");
    return typeof(return).init;
  }

  rpc_Dataset getDatasetById(int id) {
    // Your implementation goes here.
    writeln("getDatasetById called");
    return typeof(return).init;
  }

  Status startService() {
      return startService_impl();
  }

  Status stopService() {
      return stopService_impl();
  }

  Status enableZpool(string name) {
    // Your implementation goes here.
    writeln("enableZpool called");
    return typeof(return).init;
  }

  Status enableDataset(string name) {
    // Your implementation goes here.
    writeln("enableDataset called");

        return enableDataset_impl(name);
  }

  Status regitsterServer(ref const(rpc_Server) server) {
    // Your implementation goes here.

        regitsterServer_impl(server);
    writeln("regitsterServer called");
    return typeof(return).init;
  }

  Status regitsterRpcServer() {
    // Your implementation goes here.
        regitsterRpcServer_impl();
    writeln("regitsterRpcServer called");
    return typeof(return).init;
  }

  Status regitsterServerVolumes() {
    // Your implementation goes here.
    writeln("regitsterServerVolumes called");
    return typeof(return).init;
  }

  Status sendSnapshot(ref const(rpc_SendRequest) sendreq) {
    // Your implementation goes here.
    writeln("sendSnapshot called");

    return sendSnapshotTransport_impl(sendreq);
    // return sendSnapshot_impl(sendreq.snapname, sendreq.fromsnap, sendreq.destserver.fqdn);
  }

  Status recvSnapshot(ref const(rpc_RecvRequest) recvreq) {
    // Your implementation goes here.
    writeln("recvSnapshot called");

    return recvSnapshotTransport_impl(recvreq);
    // return recvSnapshot_impl(recvreq.snapname, recvreq.origin, recvreq.originserver.fqdn);
  }

  int createZpool(string name, int serverId) {
    // Your implementation goes here.
    writeln("createZpool called");
    return typeof(return).init;
  }

  int createDataset(string name, int parentId) {
    // Your implementation goes here.
    writeln("createDataset called");
    return typeof(return).init;
  }

  void zip() {
    // Your implementation goes here.
    writeln("zip called");
  }

}

void initRpc() {
  auto protocolFactory = new TBinaryProtocolFactory!();
  auto processor = new TServiceProcessor!Control(new ControlHandler);
    auto serverTransport = new TServerSocket(8535);
  auto transportFactory = new TBufferedTransportFactory;
  auto server = new TSimpleServer(
    processor, serverTransport, transportFactory, protocolFactory);
  server.serve();
}
