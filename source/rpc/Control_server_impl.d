/**
 * Collection of RPC functin implementations
 * */

module rpc.Control_server_impl;

import rpc.Control;
import rpc.rpc_types;

import std.stdio;
import std.concurrency: Tid;

import core.stdc.stdlib;
import vibe.data.bson;

import zfs;
import data;
import globals;
import system;
import transport;

void ping() {
    // Your implementation goes here.
    writeln("ping called");
}

void shutdown_impl() {
    // exit(0);
    control.shutdown();

    //writeln("shutdown called");
}

void getZpoolByName_impl(string name) {
    // Your implementation goes here.
    writeln("getZpoolByName called");
    // return typeof(return).init;
}

void getDatasetByName_impl(string name) {
    // Your implementation goes here.
    writeln("getDatasetByName called");
    // return typeof(return).init;
}

void getZpoolById_impl(int id) {
    // Your implementation goes here.
    writeln("getZpoolById called");
    // return typeof(return).init;
}

void getDatasetById_impl(int id) {
    // Your implementation goes here.
    writeln("getDatasetById called");
    // return typeof(return).init;
}

Status startService_impl() {
    try {
        g_service_running = true;
        return Status.SUCCESS;
    } catch (Exception e) {
        return Status.FAIL;
    }
}

Status stopService_impl() {
    try {
        g_service_running = false;
        return Status.SUCCESS;
    } catch (Exception e) {
        return Status.FAIL;
    }
}

void enableZpool_impl(int id) {
    // Your implementation goes here.
    writeln("enableZpool called");
    // return typeof(return).init;
}

void enableDataset_impl(int id) {
    // Your implementation goes here.
    writeln("enableDataset called");
    // return typeof(return).init;
}

void regitsterServer_impl(ref const(rpc_Server) server) {
    // Your implementation goes here.
    MongoClient db = getDbClient();
    scope(exit) closeClient(db);
    Server srv = Server(server);

    insertServer(db, srv);
    // return typeof(return).init;
}

void regitsterRpcServer_impl() {
    // Your implementation goes here.
    MongoClient db = getDbClient();
    scope(exit) closeClient(db);
    Server srv = Server(getSystemHostname(),
                        getSystemFqdn(),
                        getSystemMachId());

    insertServer(db, srv);
    // return typeof(return).init;
}

void createZpool_impl(string name, int serverId) {
    // Your implementation goes here.
    writeln("createZpool called");
    // return typeof(return).init;
}

void createDataset_impl(string name, int parentId) {
    // Your implementation goes here.
    writeln("createDataset called");
    //    return typeof(return).init;
}

/// TODO: remove
void zip_impl() {
    // Your implementation goes here.
    writeln("zip called");
}

/**
 * Implements enableDataset() RPC function
 *
 * Params:
 *   name = full name of dataset to enable backups
 * */
Status enableDataset_impl(string name) {
    if (! g_service_running)
        return Status.FAIL;

    MongoClient db = getDbClient();
    scope(exit) closeClient(db);

    Dataset ds = getDataset(db, name, control.localhost._id);

    try {
        ds.backupsEnabled = 1;
        updateItem(db, ds);

        return Status.SUCCESS;
    } catch (Exception e) {
        return Status.FAIL;
    }
}

/**
 * Implementation of sendSnapshot() from RPC service
 *
 * Takes input data from RPC request and passes to ZFS module for async send
 *
 * TODO: Status.FAIL is not being passed back to Control_server.d if service is
 * not running
 */
Status sendSnapshot_impl(string snapname, string fromsnap, string destserver) {
    if (! g_service_running){
        logError("service is not running");
        return Status.FAIL;
    }

    logDebug("rpc: sendSnapshot_impl -- starting send thread");
    Tid sendT = sendSnapAsync(snapname, fromsnap, destserver, thisTid);

    Status ret = Status.FAIL;
    receiveTimeout(dur!("seconds")( 10 ),(Status stat){
            logDebug("received status from async");
            ret = Status.SUCCESS;
        });

    return ret;
}

/**
 * Implementation of sendSnapshot() from RPC service
 *
 * Takes input data from RPC request and passes to ZFS module for async send
 *
 * TODO: Status.FAIL is not being passed back to Control_server.d if service is
 * not running
 */
Status sendSnapshotTransport_impl(rpc_SendRequest req) {
    if (! g_service_running){
        logError("service is not running");
        return Status.FAIL;
    }

    // logDebug("[rpc] - received new send req");
    // logDebug("[rpc]    transport id: %s", req.txUUID);
    // logDebug("[rpc]    snap name   : %s", req.snapname);
    // logDebug("[rpc]    dest server : %s", req.destserver.fqdn);

    NewTransport(req);
    return Status.SUCCESS;
}

/**
 * Implementation of recvSnapshot() from RPC service
 *
 * Takes input data from RPC request and passes to ZFS module for async recv
 * */
Status recvSnapshot_impl(string snapname, string origin, string destserver) {
    if (! g_service_running){
        logError("service is not running");
        return Status.FAIL;
    }

    logDebug("rpc: recvSnapshot_impl -- starting recv thread");
    Tid sendT = recvSnapAsync(snapname, origin, destserver, thisTid);

    Status ret = Status.FAIL;
    receiveTimeout(dur!("seconds")( 10 ),(Status stat){
            logDebug("received status from async");
            ret = Status.SUCCESS;
        });

    return ret;
}

/**
 * Implementation of recvSnapshot() from RPC service
 *
 * Takes input data from RPC request and passes to ZFS module for async recv
 * */
Status recvSnapshotTransport_impl(rpc_RecvRequest req) {
    if (! g_service_running){
        logError("service is not running");
        return Status.FAIL;
    }

    NewTransport(req);
    return Status.SUCCESS;
}
