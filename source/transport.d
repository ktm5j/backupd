module transport;

import std.algorithm;
import core.thread;
import std.concurrency;
import std.socket;
import core.time;
import std.uuid;

alias send = std.concurrency.send;

import vibe.core.log;

import rpc.rpc_types;
import rpc.Control_server_impl;

import globals;
import models;
import zfs;

/// Cancel a pending send/recv thread
struct CancelThread {
}

/// Ack a CancelThread
struct CancelThreadAck {
}

/// Indicate that the correct match has been made
struct MatchMade {
}

__gshared static package Socket ServiceSocket;
__gshared static package RecordBook Records;

__gshared package Tid ConnectionHandlerTid;
__gshared package Tid SocketMatcher;
__gshared package Tid RecordCleaner;

public bool TransportRunning;


enum RecordStatus {
                   NEW_RECORD = 1,
                   MATCH_MADE = 2,
                   CONNECTED = 3,
                   IN_PROGRESS = 4,
                   COMPLETED = 5
}

package struct SocketRecord {
    Socket socket;
    UUID txUUID;
    RecordStatus status = RecordStatus.NEW_RECORD;
}

package struct SendRecord {
    Tid tid;
    UUID txUUID;
    RecordStatus status = RecordStatus.NEW_RECORD;
}

package struct RecvRecord {
    Tid tid;
    UUID txUUID;
    RecordStatus status = RecordStatus.NEW_RECORD;
}

package static class RecordBook {
    __gshared Socket serviceSocket;
    __gshared SocketRecord[] pendingSockets = [];

    __gshared SendRecord[] sendRecords = [];
    __gshared RecvRecord[] recvRecords = [];

    void newSendRecord(Tid tid, UUID uuid) {
        this.sendRecords ~= SendRecord(tid, uuid);
    }

    void newRecvRecord(Tid tid, UUID uuid) {
        this.recvRecords ~= RecvRecord(tid, uuid);
    }

    void newSocketRecord(Socket sock, UUID uuid) {
        this.pendingSockets ~= SocketRecord(sock, uuid);
    }
}

public void OpenTransportSocket() {
    ZfsConfig *conf = &AppConfig.zfsConfig;
    ServiceSocket = new TcpSocket();

    ServiceSocket.setOption
        (SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, true);

    if (g_flag_debug)
        ServiceSocket.setOption
            (SocketOptionLevel.SOCKET, SocketOption.DEBUG, true);

    logDebug("opening transport socket, addr: %s, port: %d", conf.bindaddr, conf.port);
    ServiceSocket.bind(new InternetAddress(conf.bindaddr, conf.port));
    ServiceSocket.listen(10);
}

public void NewTransport(T) (T req) {
    newTransport(req);
}

private void newTransport(rpc_SendRequest req) {
    logDebug("new send transport id: %s", req.txUUID);
    logDebug("         snap name   : %s", req.snapname);
    logDebug("         dest server : %s", req.destserver.fqdn);

    Tid tid = spawn(&SendThread, req);
    logDebug("thread spawned");
    string s = req.txUUID;
    UUID id = parseUUID(s);
    logDebug("uuid created");

    Records.sendRecords ~= SendRecord(tid, id);
    logDebug("record created");
}

private void newTransport(rpc_RecvRequest req) {
    logDebug("new recv transport id: %s", req.txUUID);
    logDebug("         snap name   : %s", req.snapname);
    logDebug("         dest server : %s", req.originserver.fqdn);

    Tid tid = spawn(&RecvThread, req);
    logDebug("thread spawned");

    string s = req.txUUID;
    UUID id = parseUUID(s);

    logDebug("uuid created");

    Records.recvRecords ~= RecvRecord(tid, id);
}

public void StartTransport() {
    ConnectionHandlerTid = spawn(&ConnectionHandler, true);
    SocketMatcher = spawn(&MatchSendSockets, true);
    RecordCleaner = spawn(&RecordClean_impl, true);
}

private void RecordClean(T)(UUID id) {
    logDebug("[RecordClean] starting to process record");
    static if (is(T == SendRecord)) {
        logDebug("[RecordClean] send record");
        SendRecord inreq = SendRecord();
        inreq.txUUID = id;
        send(RecordCleaner, inreq);
        return;
    }

    static if (is(T == RecvRecord)) {
        logDebug("[RecordClean] recv record");
        RecvRecord inreq = RecvRecord();
        inreq.txUUID = id;
        send(RecordCleaner, inreq);
        return;
    }

        // logError("[RecordClean] error processing record");
        // return;

}

/// Input parameter only exists because std.conn.spawn won't work otherwise
private void RecordClean_impl(bool input) {
    while (true) {
        receive(
                (Socket s) {
                    logDebug("received Socket: ");
                },
                (SendRecord inreq) {
                    logDebug("cleaning sendreq for txid: %s", inreq.txUUID);

                    SendRecord *sr;
                    foreach (rec; Records.sendRecords) {
                        if (inreq.txUUID == rec.txUUID) {
                            sr = &rec;
                            break;
                        }
                    }

                    if (sr != null) {
                        // Records.sendRecords = Records.sendRecords.remove(sr);
                        sr.status = RecordStatus.COMPLETED;
                        send(sr.tid, CancelThread());
                    }
                },
                (RecvRecord inreq) {
                    logDebug("cleaning sendreq for txid: %s", inreq.txUUID);
                    RecvRecord *rr;
                    foreach (rec; Records.recvRecords) {
                        if (inreq.txUUID == rec.txUUID) {
                            rr = &rec;
                            break;
                        }
                    }

                    if (rr != null) {
                        // Records.recvRecords = Records.recvRecords.remove(rr);
                        rr.status = RecordStatus.COMPLETED;

                    }
                },
                (CancelThread m) {
                    logDebug("Stopping ", thisTid, "...");
                    return;
                }
                );
    }
}

/// Input parameter only exists because std.conn.spawn won't work otherwise
// private void UpdateRecord(bool input) {
//     receive(
//         (Socket s) {
//           logDebug("received Socket: ");
//         },
//         (UUID id) {
//             logDebug("cleaning sendreq for txid: %s", id);

//             SendRecord *sr;
//             foreach (rec; Records.sendRecords) {
//                 if (id == rec.txUUID) {
//                     sr = &rec;
//                     break;
//                 }
//             }

//             if (sr != null) {
//                 Records.sendRecords.remove(sr);
//             }


//         },
//         (CancelThread m) {
//           logDebug("Stopping ", thisTid, "...");
//           return;
//         }
//       );
// }

/// Input parameter only exists because std.conn.spawn won't work otherwise
private void ConnectionHandler(bool input) {
    while (true) {
        Socket client = ServiceSocket.accept();

        ubyte[16] recvBytes;
        client.receive(recvBytes);

        UUID recvId = UUID(recvBytes);

        Records.pendingSockets ~= SocketRecord(client, recvId);
        
    }
}

/// Input parameter only exists because std.conn.spawn won't work otherwise
private void MatchSendSockets(bool input) {
    while (true) {
        logDebug("MatchSockets looping");

        try {
        foreach (sockreq; Records.pendingSockets) {
            if ( &sockreq != null && sockreq.status == RecordStatus.NEW_RECORD ) {

                foreach (sendreq; Records.sendRecords) {
                    if (sockreq.txUUID == sendreq.txUUID) {
                        sendreq.status = RecordStatus.MATCH_MADE;
                        sockreq.status = RecordStatus.MATCH_MADE;

                        std.concurrency.send(sendreq.tid, cast(shared) sockreq.socket);
                    }
                }
            }

            // TODO Handle other statuses
            //else if (sockreq.status == RecordStatus.) {}
        }
        }  catch (Exception e) { }

        receiveTimeout(dur!("seconds")( 1 ),(CancelThread msg){
                logDebug("received cancelthread message");
                return;
            });
    }
}

private void MatchRecvSockets() {
    while (true) {
        foreach (recvreq; Records.recvRecords) {
            logDebug("Recv req processing ;->");
            
        }
    }

}

/**
 * Function to spawn as thread for use in sending snapshots
 *
 * Will loop endlessly awaiting receipt of a matching socket or a `CancelThread`
 * message. If `CancelThread` is receive()'d then thread will reply with
 * `CancelThreadAck`.
 */
private void SendThread( rpc_SendRequest req) {
    logDebug("spawned -- new SendThread - tid: %s", thisTid);

    ushort tries = 3;
    ushort attempt = 0;

    string str1 = req.txUUID;
    UUID reqId = parseUUID(str1);

    bool looping = true;
 EXIT:
    while (looping) {
        logDebug("SendThread - tid: %s looping", thisTid);
        receive
            (
             (shared Socket s) {
                 Socket sock = cast(Socket) s;

                 // TODO is redundant? can safely reuse str1?
                 logDebug("sending bytes");
                 string str1 = req.txUUID;
                 UUID id = parseUUID(str1);
                 sock.send(id.data);

                 if (id == reqId) {
                     logDebug("match made, starting lzc_send");
                     Status stat = zfs.sendSnap_impl(req.snapname, req.fromsnap, req.destserver.fqdn, sock);

                     if (stat == Status.FAIL) {
                         logDebug("send status FAIL");
                         sock.shutdown(SocketShutdown.BOTH);
                         sock.close();

                         if (attempt == tries)
                             looping = false;
                         else
                             attempt++;
                     }

                     if (stat == Status.SUCCESS) {
                         logDebug("send status SUCCESS ");
                         RecordClean!SendRecord(id);

                         sock.shutdown(SocketShutdown.BOTH);
                         sock.close();
                         looping = false;
                     }
                 }
             },
             (CancelThread c) { logDebug("caught CancelThread signal"); looping = false; }
             );
    }
}

/**
 * Function to spawn as thread for use in receiving snapshots
 *
 * TODO: Figure out how to timeout a connection attempt so that we can implement
 * thread cancellation
 */
private void RecvThread(rpc_RecvRequest req) {
    logDebug("spawned -- new RecvThread - tid: %s", thisTid);

    ushort port = AppConfig.zfsConfig.port;
    string addr = req.originserver.fqdn;

    logDebug("attemting to connect to addr: %s, port: %d", addr, port);
    Socket sock = new TcpSocket();
    sock.connect(new InternetAddress(addr, port));

    string str = req.txUUID;
    UUID u = parseUUID(str);

    while (true) {
        logDebug("RecvThread - tid: %s looping", thisTid);
        sock.send(u.data);

        logDebug("receiving bytes");
        ubyte[16] senderBytes;
        sock.receive(senderBytes);

        UUID senderId = UUID(senderBytes);

        if (u == senderId) {
            logDebug("match made, starting lzc_receive");
            Status stat = zfs.recvSnap_impl(req.snapname, req.origin, req.originserver.fqdn, sock);

            if (stat == Status.FAIL) {
                logDebug("recv status FAIL");
            }

            if (stat == Status.SUCCESS) {
                logDebug("recv status SUCCESS ");
                RecordClean!RecvRecord(senderId);
                sock.close();
                return;
            }
        }
        Thread.sleep( dur!("seconds")( 1 ) );
    }
}
