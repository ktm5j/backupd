module models.Server;

import vibe.db.mongo.mongo;
import vibe.data.serialization : name;

import rpc.rpc_types;

/**
 * Server represents a system running `backupd`. Servers are not automatically
 * registered into the database, this must be done via the Controller Class
 */
struct Server {
    BsonObjectID _id;

    /// Hostname of server
    string name;

    /// Fully qualified domain name of server
    string fqdn;

    /// Machine ID taken from /etc/machine-id
    string machId;

    this(string _name, string _fqdn, string _machId) {
        this._id = BsonObjectID.generate();
        this.name = _name;
        this.fqdn = _fqdn;
        this.machId = _machId;
    }

    rpc_Server toRpc() {
        return rpc_Server(this._id.toString(), this.name, this.fqdn, this.machId);
    }

    this(rpc_Server rpcStruct) {
        if (rpcStruct._id == "")
            this._id = BsonObjectID.generate();
        else
            this._id = BsonObjectID.fromString(rpcStruct._id);

        this.name = rpcStruct.name;
        this.fqdn = rpcStruct.fqdn;
        this.machId = rpcStruct.machId;
    }

    void genId() {
        this._id = BsonObjectID.generate();
    }

    this(Json inj) {
        string idstr = inj["_id"].get!string;

        this._id = BsonObjectID.fromString(idstr);
        this.name = inj["name"].get!string;
        this.fqdn = inj["fqdn"].get!string;
        this.machId = inj["machId"].get!string;
    }

}
