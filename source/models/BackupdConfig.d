module models.BackupdConfig;

struct BackupdConfig {
    RpcConfig rpcConfig;
    DatabaseConfig databaseConfig;
    ZfsConfig zfsConfig;
}

struct DatabaseConfig {
    string username;
    string password;
    string hostAddr;
    ushort port;
    string database;
}

struct RpcConfig {
    ushort port;
}

struct ZfsConfig {
    string bindaddr;
    ushort port;
}
