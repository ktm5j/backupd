module models.BsonExt;

import std.datetime: TimeOfDay;

struct BsonTimeOfDay {
    ubyte hour;
    ubyte minute;
    ubyte second;

    this(TimeOfDay tod) {
        this.hour = tod.hour;
        this.minute = tod.minute;
        this.second = tod.second;
    }
}
