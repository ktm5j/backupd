module models.Snapshot;

import vibe.db.mongo.mongo;
import vibe.data.serialization : name;

import rpc.rpc_types;

import std.string : split;

struct Snapshot {
    BsonObjectID _id;

    BsonObjectID serverId;
    string snapname;
    string datasetName;

    uint zStatus;
    uint backupStatus;

    this(string _snapname, BsonObjectID _serverId) {
        this._id = BsonObjectID.generate();
        this.snapname = _snapname;

        this.serverId = _serverId;

        string[] parts = this.snapname.split("@");

        if (parts == null || parts.length < 1)
            this.datasetName = "";
        else
            this.datasetName = parts[0];


    }

    void genId() {
        this._id = BsonObjectID.generate();
    }

}

unittest {
    Snapshot ss = Snapshot("test/snap@now", BsonObjectID.generate());

    assert(ss.datasetName == "test/snap");
}
