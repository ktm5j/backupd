module models.Zpool;

import vibe.db.mongo.mongo;
import vibe.data.serialization : name;

struct Zpool {
    BsonObjectID _id;

    string name;
    BsonObjectID serverId;
    int zStatus;

    this(string _name, BsonObjectID _serverId) {
        this._id = BsonObjectID.generate();
        this.name = _name;
        this.serverId = _serverId;

        this.zStatus = 0;
    }

    void genId() {
        this._id = BsonObjectID.generate();
    }

    this(Json inj) {
        string idstr = inj["_id"].get!string;

        this._id = BsonObjectID.fromString(idstr);
        this.name = inj["name"].get!string;
        string str = inj["serverId"].get!string;
        this.serverId = BsonObjectID.fromString(str);
        this.zStatus = inj["zStatus"].get!int;
    }

}
