module models.ZCollection;

import models.Zpool;
import models.Dataset;
import models.Server;

struct ZCollection {
    Server host;
    Zpool[] zpools;
    Dataset[] datasets;

    void add(Zpool z) {
        this.zpools ~= [ z ] ;
    }

    void add(Dataset z) {
        this.datasets ~= [ z ] ;
    }
}
