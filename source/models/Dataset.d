module models.Dataset;

import vibe.db.mongo.mongo;
import vibe.data.serialization : name;

struct Dataset {
    BsonObjectID _id;

    string name;
    string fullname;

    BsonObjectID serverId;

    int zStatus;
    int backupStatus;
    int backupsEnabled;

    this(string _name, string _fullname, BsonObjectID _serverId) {
        // this._id = BsonObjectID.generate();
        this.name = _name;
        this.fullname = _fullname;

        this.zStatus = 0;
        this.backupStatus = 0;
        this.backupsEnabled = 0;

        this.serverId = _serverId;
    }

    void genId() {
        this._id = BsonObjectID.generate();
    }

    this(Json inj) {
        string idstr = inj["_id"].get!string;

        this._id = BsonObjectID.fromString(idstr);
        this.name = inj["name"].get!string;
        this.name = inj["fullname"].get!string;
        this.zStatus = inj["zStatus"].get!int;
        this.backupStatus = inj["backupStatus"].get!int;
        this.backupsEnabled = inj["backupsEnabled"].get!int;
    }

}
