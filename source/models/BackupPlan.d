module models.BackupPlan;

import models.BsonExt;

import vibe.db.mongo.mongo: BsonObjectID;
import std.datetime: TimeOfDay;

struct BackupPlan {
    BsonObjectID _id;

    BsonObjectID originServerId;
    BsonObjectID destinationServerId;

    BsonObjectID datasetId;
    BsonObjectID destinationDatasetId;

    BsonTimeOfDay[] eventSchedule;

    this(BsonObjectID _originServerId,
         BsonObjectID _destinationServerId,
         BsonObjectID _datasetId,
         BsonObjectID _destinationDatasetId) {
        this.originServerId = _originServerId;
        this.destinationServerId = _destinationServerId;
        this.datasetId = _datasetId;
        this.destinationDatasetId = _destinationDatasetId;
    }

    void addTime(TimeOfDay tod) {
        this.eventSchedule ~= BsonTimeOfDay(tod);
    }

}

