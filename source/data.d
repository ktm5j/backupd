
module data;

public import vibe.d;

import std.format;
import std.stdio;

import globals : AppConfig;

alias dbconfig = AppConfig.databaseConfig;

public import models;

// private const connUri = "postgresql://ktm5j:f00bar@10.192.43.120:5432/vibe_test";
private const connFmt = "postgresql://%s:%s@%s:%d/%s";

// MongoClient client;

MongoClient getDbClient() {
    return connectMongoDB(AppConfig.databaseConfig.hostAddr);
}

void closeClient(MongoClient _client) {
    _client.cleanupConnections();
}

void insertDataset(MongoClient _client, Dataset ds) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Dataset"));

    try {
        coll.insert(ds);
    } catch (MongoDBException e) {
        logWarn("record exists");
    }
}

Dataset getDataset(MongoClient _client, BsonObjectID id) {
    auto coll = _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database, "Dataset"));

    return coll.findOne!Dataset(["_id":id]);
}

Dataset getDataset(MongoClient _client, string name, BsonObjectID serverId) {
    auto coll = _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database, "Dataset"));

    return coll.findOne!Dataset(Bson(["fullname": Bson(name), "serverId": Bson(serverId)]));
}

MongoCursor!Dataset getServerDatasets(MongoClient _client, BsonObjectID serverId) {
    auto coll = _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database, "Dataset"));

    return coll.find!Dataset(Bson(["serverId": Bson(serverId)]));
}

void removeDataset(MongoClient _client, Dataset ds) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Dataset"));

    try {
        coll.remove(ds);
    } catch (MongoDBException e) {
        logWarn("error deleteing ds document for: %s", ds.fullname);
    }
}

void insertSnapshot(MongoClient _client, Snapshot ss) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Snapshot"));

    try {
        coll.insert(ss);
    } catch (MongoDBException e) {
        logWarn("record exists");
    }
}

void removeSnapshot(MongoClient _client, Snapshot ss) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Snapshot"));

    try {
        coll.remove(ss);
    } catch (MongoDBException e) {
        logWarn("error deleteing ds document for: %s", ss.snapname);
    }
}

MongoCursor!Snapshot getDatasetSnapshots(MongoClient _client, Dataset ds) {
    auto coll = _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database, "Snapshot"));

    return coll.find!Snapshot(Bson(["datasetName": Bson(ds.fullname), "serverId": Bson(ds.serverId)]));
}

void insertZpool(MongoClient _client, Zpool zp) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Zpool"));

    try {
        coll.insert(zp);
    } catch (MongoDBException e) {
        logWarn("record exists");
    }
}

Zpool getZpool(MongoClient _client, BsonObjectID id) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Zpool"));

    return coll.findOne!Zpool(["_id":id]);
}

void insertServer(MongoClient _client, Server srv) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Server"));

    try {
        coll.insert(srv);
    } catch (MongoDBException e) {
        logWarn("record exists");
    }
}

void insertBackupPlan(MongoClient _client, BackupPlan bp) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "BackupPlan"));

    try {
        coll.insert(bp);
    } catch (MongoDBException e) {
        logWarn("record exists");
    }
}

MongoCursor!BackupPlan getBackupPlans(MongoClient _client, BsonObjectID serverId) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Zpool"));

    return coll.find!BackupPlan(["originServerId":serverId]);
}

Server getServer(MongoClient _client, BsonObjectID id) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Server"));

    return coll.findOne!Server(["_id":id]);
}

Server getServerFqdn(MongoClient _client, string _fqdn) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Server"));

    return coll.findOne!Server(["fqdn":_fqdn]);
}

Server getServerMach(MongoClient _client, string _machId) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Server"));

    return coll.findOne!Server(["machId":_machId]);
}

void insertZCollection(MongoClient _client, ZCollection zc) {
    for (int i = 0; i < zc.zpools.length; i++) {
        insertZpool(_client, zc.zpools[i]);
    }

    for (int i = 0; i < zc.datasets.length; i++) {
        insertDataset(_client, zc.datasets[i]);
    }
}

void enableUpdates(MongoClient _client, BsonObjectID id) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Dataset"));

    coll.update(["_id":id],
                ["$set":["backupsEnabled": 1]]);
    // coll.findAndModify(["_id":id],["$set" : ["backupsEnabled", "1"]]);

}

void updateItem(MongoClient _client, Dataset ds) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Dataset"));

    coll.update(["_id":ds._id],
                ["$set":ds]);

}

void updateItem(MongoClient _client, Server sv) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Server"));

    coll.update(["_id":sv._id],
                ["$set":sv]);

}

void updateItem(MongoClient _client, Zpool zp) {
    auto coll =
        _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
                                            "Zpool"));

    coll.update(["_id":zp._id],
                ["$set":zp]);

}

// void disableUpdates(MongoClient _client, string ds) {
//     auto coll =
//         _client.getCollection(format!"%s.%s"(AppConfig.databaseConfig.database,
//                                             "Dataset"));

//     coll.update(["fullname": ds],
//                 ["$set":["backupsEnabled", Bson(0)]]);

// }
