# Testing Send and Receive

## Kali & Ztest01

**Start services**

```shell
./backupd --dbaddress 10.194.105.1 --debug
```

**Send on kali**

```shell
./client send z/testing/test1@t01 ztest01 --tx f6368b43-112b-43b0-b416-02cdb8ac737a
```

**Recv ztest01**

```shell
./client --server ztest01 recv t/test/test1@t01 kali.sandbox.zone
```




