# -*- mode: ruby -*-
# vi: set ft=ruby :

def errorOut(msg)
  puts "[ Provisioning Error ] " + msg

  exit!(1)
end

$script_base = <<-EOF
LOGFILE=/root/provisioning.log

id  2>&1 >> $LOGFILE

function errorOut() {
    echo "[ Provisioning Error ]"
    exit 1
}

echo "provisioning system"                 2>&1 >> $LOGFILE
echo "installing updates"                  2>&1 >> $LOGFILE
dnf update   -y                            2>&1 >> $LOGFILE || errorOut
dnf install  -y  curl llvm7.0 clang7.0-devel clang7.0-libs clang ntpdate     \
                 libevent libevent-devel openssl openssl-libs openssl-devel          2>&1 >> $LOGFILE || errorOut

dnf install  -y  http://download.zfsonlinux.org/fedora/zfs-release.fc30.noarch.rpm   2>&1 >> $LOGFILE || errorOut
gpg --quiet --with-fingerprint /etc/pki/rpm-gpg/RPM-GPG-KEY-zfsonlinux               2>&1 >> $LOGFILE || errorOut

dnf install  -y  kernel-devel zfs zfs-dracut libzfs2 libzfs2-devel       2>&1 >> $LOGFILE || errorOut

curl -fsS https://dlang.org/install.sh | bash -s -- dmd -p /opt/dlang    2>&1 >> $LOGFILE || errorOut
curl -fsS https://dlang.org/install.sh | bash -s -- ldc -p /opt/dlang    2>&1 >> $LOGFILE || errorOut

chown -R vagrant: /opt

KVERS=$(ls /boot/ | grep vmlinuz-5 | tail -1 | cut -f 2- -d'-')
echo building zfs mods for $KVERS                                        2>&1 >> $LOGFILE || errorOut
dkms autoinstall -k $KVERS --force                                       2>&1 >> $LOGFILE || errorOut

ntpdate -u time-a-g.nist.gov

date > /root/vagrant_provisioned_at

EOF

$script_w_mongo = $script_base + <<-EOF
cat > /etc/yum.repos.d/mongo.repo << EOIF
[Mongodb]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/amazon/2/mongodb-org/4.2/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc
EOIF

curl -L https://www.mongodb.org/static/pgp/server-4.2.asc | gpg --quiet --with-fingerprint

dnf install -y mongodb-org
EOF

Vagrant.configure("2") do |config|

  config.vm.define :ztest01 do |ztest01|

    ztest01.vm.box = "ktm5j/fedora30"
    ztest01.vm.box_version = "0.0.1"

    ztest01.vm.hostname = "ztest01"

    ztest01.vm.provider :libvirt do |libvirt|
      libvirt.cpus = 4
      libvirt.memory = 4096
      libvirt.storage :file, :size => '2G'
    end

    ztest01.vm.provision "shell", inline: $script_w_mongo
    ztest01.vm.synced_folder './', '/code', type: '9p', disabled: false, accessmode: "squash", owner: "1000"

    ztest01.trigger.after [:provision] do |t|
      t.name = "Reboot after provisioning"
      t.run = { :inline => "vagrant reload" }
    end
  end

  config.vm.define :ztest02 do |ztest02|

   ztest02.vm.box = "ktm5j/fedora30"
   ztest02.vm.box_version = "0.0.1"

   ztest02.vm.hostname = "ztest02"

   ztest02.vm.provider :libvirt do |libvirt|
      libvirt.cpus = 4
      libvirt.memory = 4096
      libvirt.storage :file, :size => '2G'
    end

   ztest02.vm.provision "shell", inline: $script_base
   ztest02.vm.synced_folder './', '/code', type: '9p', disabled: false, accessmode: "squash", owner: "1000"

   ztest02.trigger.after [:provision] do |t|
     t.name = "Reboot after provisioning"
     t.run = { :inline => "vagrant reload" }
   end

  end


end
